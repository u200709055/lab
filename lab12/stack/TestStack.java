package stack;

public class TestStack {
    public static void main(String[] args) {
        //Stack stack= new StackImpl(); //StackImpl class implements Stack interface.
        testStack(new StackImpl());

        testStack(new StackArrayImpl());
    }
    public static void testStack(Stack stack){
        stack.push(5);
        stack.push(3);
        stack.push(6);
        stack.push("hello");
        stack.push(22);  //22 is top.

        while (!stack.empty()){
            System.out.println(stack.pop());
        }
    }
}
