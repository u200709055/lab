package tictactoe;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        Board board = new Board();

        System.out.println(board);
        int col = 0;
        int row = 0;
        while (!board.isEnded()) {
            boolean invalidRow = false;
            int player;
            row = 0;
            do {
                player = board.getCurrentPlayer();

                System.out.print("Player " + player + " enter row number:");
                try {
                    row = Integer.valueOf(reader.nextLine());
                    System.out.println("Valid Integer");
                    invalidRow = false;
                } catch (NumberFormatException numberFormatException) {
                    System.out.println("Invalid Integer");
                    invalidRow = true;
                }
            } while (invalidRow);
            boolean invalidColumn = false;

            do {
                System.out.print("Player " + player + " enter column number:");
                try {
                    col = Integer.valueOf(reader.nextLine());
                    System.out.println("Valid Integer");
                    invalidColumn = false;
                } catch (NumberFormatException numberFormatException) {
                    System.out.println("Invalid Integer");
                    invalidColumn = true;
                }
            } while (invalidColumn);
            try {
                board.move(row, col);
            } catch (InvalidMoveException e) {
                System.out.println(e.getMessage());
            }
            System.out.println(board);

        }


        reader.close();
    }
}



