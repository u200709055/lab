package generics;

import java.util.List;

public interface Stack<T extends Number> {
    void push(T item);

    T pop();

    boolean empty();
     List<T> toList();
    default void addAll(Stack< ? extends T> aStack) {  //default kelimesi abtract methodun gövdeli olduğunu gösterir.
        List< ? extends T> list= aStack.toList(); //? extend T. T her şeyi kabul eder.
        for (T item:list){
            push(item);
        }
    }
}
