package generics;

public class TestStack {
    public static void main(String[] args) {

       //testStack(new StackImpl<Integer>());
        testStack(new StackArrayImpl<Number>());
    }
    public static void testStack(Stack <Number>stack){

        stack.push(5);
        stack.push(2);
        stack.push(11);
        stack.push(2.0);
        stack.push(10000);
        //stack.push("hello");
        stack.push(23);  //23 is top.
        System.out.println("stack1:"+stack.toList());
        Stack<Integer> stack2=new StackImpl<>();
        stack2.push(44);
        stack2.push(11);
        stack2.push(55);
        System.out.println("stack2:"+stack.toList());
        stack.addAll(stack2);
        System.out.println("stack1:"+stack.toList());

        while (!stack.empty()){
            System.out.println(stack.pop());
        }
        System.out.println(stack.toList());
    }
}
