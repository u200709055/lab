import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		
		int attempt=1;
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
		
		int guess = reader.nextInt(); //Read the user input

		while (guess!=number && guess!=-1){
			attempt++;
			System.out.println("sorry");
			if (guess<number){
				System.out.println("mine is greater than your guess.");
			}
			else if (guess > number){

				System.out.println("mine is less than your guess.");
			}
			System.out.println("type -1 to guit or guess another.");
			guess= reader.nextInt();
		}
		if (guess==number){
			System.out.println("Congratulations! You won after "+ attempt + " "+"attemps.");
		}
		else {
			System.out.println("sorry number was the:"+ number);
		}
		
		
		
		reader.close(); //Close the resource before exiting
	}
	
	
}