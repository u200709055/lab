public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle= new Rectangle(new Point(3,2),10,12);
        System.out.println("Area of rectangle:"+ rectangle.area());
        System.out.println("Perimeter of rectangle:"+ rectangle.perimeter());
        Point[] corners= rectangle.corners();
        for (int i=0; i< corners.length; i++){
            Point p= corners[i];
            if (p==null)
                System.out.println("p is null");
            else{
                System.out.println("corner"+ (i+1) + " x="+ p.getxCoord()
                        + " y="+ p.getyCoord());
            }
        }

    }
}
