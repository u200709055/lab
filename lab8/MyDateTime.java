import java.util.Date;

public class MyDateTime{
    MyDate date;
    MyTime time;

    public MyDateTime(MyDate date, MyTime time) {

        this.date = date;
        this.time = time;
    }
/*    public String toString(){

    }*/

    @Override
    public String toString() {
        return date+ " "+ time;
    }

    public void incrementDay() {
        date.incrementDay();
    }

    public void incrementHour() {
       incrementHour(1);
    }

    public void incrementHour(int diff) {
        int dayDiff= time.incrementHour(diff);
        if (dayDiff<0)
            date.decrementDay(-dayDiff);
        else
            date.incrementDay(dayDiff);
    }

    public void decrementHour(int diff) {
        incrementHour(-diff);
    }

    public void incrementMinute(int diff) {
       int dayDiff= time.incrementMinute(diff);
       if (dayDiff<0)
           date.decrementDay(-dayDiff);
       else
           date.decrementDay(dayDiff);
    }

    public void decrementMinute(int diff) {
        incrementMinute(-30);
    }

    public void incrementYear(int diff) {
        date.incrementYear(diff);
    }

    public void decrementDay() {
        date.decrementDay();
    }

    public void decrementYear() {
        date.decrementYear();
    }

    public void decrementMonth() {
        date.decrementMonth();
    }

    public void incrementDay(int i) {
        date.incrementDay(i);
    }

    public void decrementMonth(int i) {
        date.decrementMonth(i);
    }

    public void decrementDay(int i) {
        date.decrementDay(i);
    }

    public void incrementMonth(int i) {
        date.incrementMonth(i);
    }

    public void decrementYear(int i) {
        date.decrementYear(i);
    }

    public void incrementMonth() {
        date.incrementMonth();
    }

    public void incrementYear() {
        date.incrementYear();
    }

    public boolean isAfter(MyDateTime anotherDateTime) {
        int a = Integer.parseInt(time.toString().replaceAll(":", "")) ;

        int b = Integer.parseInt(anotherDateTime.time.toString().replaceAll(":", ""));
        return a>b;
    }

    public boolean isBefore(MyDateTime anotherDateTime) {
        int a = Integer.parseInt(time.toString().replaceAll(":", "")) ;

        int b = Integer.parseInt(anotherDateTime.time.toString().replaceAll(":", ""));
       return a<b;
    }


    public String dayTimeDifference(MyDateTime anotherDateTime) {

       int hours=  ((time.hour-anotherDateTime.time.hour)+ (time.minute-anotherDateTime.time.minute)/60);
       int hours1=  ((anotherDateTime.time.hour-time.hour)+(anotherDateTime.time.minute-time.minute)/60);

       return (time.hour>anotherDateTime.time.hour && time.minute>anotherDateTime.time.minute)? hours+ "hour(s)":hours1+"hour(s)";

    }
}

