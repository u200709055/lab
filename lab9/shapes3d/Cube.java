package shapes3d;

import shapes2d.Square;

public class Cube extends Square {
    public Cube(double side) {
        super(side);
    }
    public double area(){
        return new Square(side).area()*6;
    }
    public double volume(){
        return side*side*side;
    }

    @Override
    public String toString() {
        return "Cube's side= "+side;
    }
}
