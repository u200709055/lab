package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {
    protected double height;
    public Cylinder(double radius, double height) {
        super(radius);
        this.height=height;

    }
    public double area(){
        return Math.PI*radius*radius;
    }
    public double volume(){
        return Math.PI*radius*radius*height;
    }

    @Override
    public String toString() {
        return "Cylinder's height="+ height+
                "\nCylinder's radius="+radius;
    }
}
